Acme Bank is a VoiceXML application.

# Requirements
1. VoiceXML browser (typically an IVR)
2. Speech recognizer
3. ArmorVox server

# Build
`mvn clean package`

# Run
`java -jar acme_bank.jar`

# Endpoint
`http://localhost:8080/demo`